package SeaRange;

public class Fish {
    public String name;
    public Integer lifespan;
    public Boolean viviparous;
    public Boolean predatory;
    private Integer weight;
    public Fish( String name, Integer lifespan, Boolean viviparous,
                Boolean predatory, Integer weight)
    {
        this.name = name;
        this.lifespan = lifespan;
        this.viviparous = viviparous;
        this.predatory = predatory;
        this.weight = weight;
    }


    public Integer GetWeight()
    {
        return weight;
    }
    public void SetWeight (Integer weight)
    {
    this.weight = weight;
    }
public void showFish()
{
    System.out.println( "Hi, I m a " + name + ". I would like to tell you some info about myself"
            + "\nLifespan: " + lifespan +
            "\nVivaparous: " + viviparous+
            "\nPredatory: " + predatory+
            "\nWeight: "+ GetWeight() + "\n\n");
}
}
