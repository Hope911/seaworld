package SeaRange;

public class Seaweed {
    public String name;
    public String green;
    public Seaweed( String name,  String green)
    {
        this.name = name;
        this.green = green;

    }
    public void showFish() {
        System.out.println("Hi, I m a " + name + ". I would like to tell you some info about myself"
                + ". I have " + green + " colour." + "\n\n");
    }
}
